var poshldr = document.getElementById("poshldr");
var depthldr = document.getElementById("depthldr");
var posArray = new Array();
var deptArray = new Array();

var myVar = localStorage['myKey'] || 'defaultValue';
console.log(myVar);
var admincomp = "";

var CheckRef = firebase.database().ref().child("Users").child(myVar);
CheckRef.once("value", function(snapshot){
      admincomp = snapshot.val().admincomp;
      console.log(admincomp);
   });

setTimeout(function(){
var DeptRef = firebase.database().ref().child("Company").child(admincomp).child("Department");
DeptRef.once("value", function(snapshot){
   snapshot.forEach(function(value){
      var deptname = value.val().deptname;
      deptArray.push(String(deptname));
      console.log(deptname);
   });
 });

 var DeptRef = firebase.database().ref().child("Company").child(admincomp).child("Position");
 DeptRef.once("value", function(snapshot){
    snapshot.forEach(function(value){
       var posname = value.val().posname;
       posArray.push(String(posname));
       console.log(posname);
    });
  });
}, 5000);

setTimeout(function(){
   for(var x = 0; x < posArray.length; x++){
       poshldr.options.add(new Option(posArray[x], posArray[x]));
       console.log(posArray[x]);
   }

   for(var i = 0; i < deptArray.length; i++){
       depthldr.options.add(new Option(deptArray[i], deptArray[i]));
       console.log(deptArray[i]);
   }
}, 10000);


//AccountInfo
var email = document.getElementById("email");
var password = document.getElementById("password");

function AddEmployee(){
  var firebaseRef = firebase.database().ref();
  firebase.auth().createUserWithEmailAndPassword(email.value,password.value).then((success) =>     {
  window.alert("Employee Added!");
  var uid = firebase.auth().currentUser.uid;
  var UsersRoot = firebaseRef.child("Users").child(firebase.auth().currentUser.uid);

  UsersRoot.set({
  id: firebase.auth().currentUser.uid,
  name: document.getElementById("name").value,
  birthday: document.getElementById("birthday").value,
  address:  document.getElementById("address").value,
  gender: document.getElementById("gender").value,
  status: document.getElementById("status").value,
  sss: document.getElementById("sss").value,
  philhealth: document.getElementById("philhealth").value,
  pagibigno: document.getElementById("pagibigno").value,
  bankacct: document.getElementById("bankacct").value,
  datehired: document.getElementById("datehired").value,
  email: document.getElementById("email").value,
  password: document.getElementById("password").value,
  contact: document.getElementById("contact").value,
  position: document.getElementById("poshldr").value,
  department: document.getElementById("depthldr").value,
  educattain: document.getElementById("educattainment").value,
  degree: document.getElementById("degree").value,
  company: document.getElementById("company").value,
  pposition: document.getElementById("pposition").value,
  startdate: document.getElementById("startdate").value,
  enddate: document.getElementById("enddate").value,
  evalstatus: "false"
  })
  .catch(function (error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      console.log("error code: " + errorCode);
      console.log("error message: " + errorMessage);
      window.alert(errorMessage);
  });

}).catch(function (error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      console.log("error code: " + errorCode);
      console.log("error message: " + errorMessage);
      window.alert(errorMessage);
  });
}
