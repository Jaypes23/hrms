// function AddCompany(){
//   var firebaseRef = firebase.database().ref();
//   window.alert("Company Added!");
//
//   var randomNum = Math.floor(Math.random() * 1000) + 0;
//   var name = document.getElementById("compname").value;
//
//   var CompRoot = firebaseRef.child("Company").child(name);
//   CompRoot.set({
//   compid: String(randomNum),
//   compname: document.getElementById("compname").value,
//   compdesc: document.getElementById("compdesc").value,
//   complocation:  document.getElementById("complocation").value
//   })
//   .catch(function (error) {
//       // Handle Errors here.
//       var errorCode = error.code;
//       var errorMessage = error.message;
//       console.log("error code: " + errorCode);
//       console.log("error message: " + errorMessage);
//       window.alert(errorMessage);
//   });
// }

var email = document.getElementById("adminemail");
var password = document.getElementById("adminpassword");

function AddCompany(){
  var firebaseRef = firebase.database().ref();
  firebase.auth().createUserWithEmailAndPassword(email.value,password.value).then((success) =>     {
  window.alert("Company Added!");
  var uid = firebase.auth().currentUser.uid;
  var UsersRoot = firebaseRef.child("Users").child(firebase.auth().currentUser.uid);

  UsersRoot.set({
  id: firebase.auth().currentUser.uid,
  adminname: document.getElementById("adminname").value,
  adminemail: document.getElementById("adminemail").value,
  adminpassword:  document.getElementById("adminpassword").value,
  admincomp: document.getElementById("compname").value,
  userstatus: "admin"
  })
  .catch(function (error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      console.log("error code: " + errorCode);
      console.log("error message: " + errorMessage);
      window.alert(errorMessage);
  });

}).catch(function (error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      console.log("error code: " + errorCode);
      console.log("error message: " + errorMessage);
      window.alert(errorMessage);
  });

  var randomNum = Math.floor(Math.random() * 1000) + 0;
  var compname = document.getElementById("compname").value;

  var CompRoot = firebaseRef.child("Company").child(compname).child("Company Information");
  CompRoot.set({
  compid: String(randomNum),
  compname: document.getElementById("compname").value,
  compdesc: document.getElementById("compdesc").value,
  complocation:  document.getElementById("complocation").value
  })
  .catch(function (error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      console.log("error code: " + errorCode);
      console.log("error message: " + errorMessage);
      window.alert(errorMessage);
  });

}
