 var PostRef = firebase.database().ref().child("Position");
 PostRef.orderByChild("postname").once("value", function(snapshot){
   $('#tb').DataTable({
     columns: [
       {title: "ID"},
       {title: "Name"},
       {title: "Description"},
       {title: "Department"},
       {title: "Working Hours"},
     ]
   });
   var m = $('#tb').DataTable();
   snapshot.forEach(function(value){
     var id = value.val().posid;
     var name = value.val().posname;
     var desc = value.val().posdesc;
     var dept = value.val().posdept;
     var hours = value.val().poswork;
     m.row.add([
       id,
       name,
       desc,
       dept,
       hours
     ]).draw(false);
   });
 });
