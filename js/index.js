
var emailhldr = document.getElementById("emailhldr");
var passwordhldr = document.getElementById("passwordhldr");

function loginClick(){
  firebase.auth().signInWithEmailAndPassword(emailhldr.value,passwordhldr.value)
  .then((success) => {
    console.log("log in user");
    var uid = firebase.auth().currentUser.uid;

    var CheckRef = firebase.database().ref().child("Users").child(uid);
    CheckRef.once("value", function(snapshot){
       snapshot.forEach(function(value){
          var userstatus = value.val();
          console.log(userstatus);
          if(userstatus == "admin"){
            alert("HR Admin Login Success!");
            window.location ="starter.html";

            localStorage['myKey'] = String(uid);
          }
          else if(userstatus == "superadmin"){
            alert("Super Admin Login Success!");
            window.location ="superadmin.html";
          }
       });
     });



  })
  .catch(function(error) {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;
    console.log("error code: " + errorCode);
    console.log("error message: " + errorMessage);
    window.alert(errorMessage);
  });
}


$('.login-input').on('focus', function() {
  $('.login').addClass('focused');
});

$('.login').on('submit', function(e) {
  e.preventDefault();
  $('.login').removeClass('focused').addClass('loading');
});
