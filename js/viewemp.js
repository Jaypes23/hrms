
  var UsersRef = firebase.database().ref().child("Users");
  UsersRef.orderByChild("name").once("value", function(snapshot){

    $('#tb').DataTable({
      columns: [
        {title: "ID"},
        {title: "Name"},
        {title: "Position"},
        {title: "Department"},
      ]
    });
    var m = $('#tb').DataTable();
    snapshot.forEach(function(value){
      var id = value.val().id;
      var name = value.val().name;
      var position = value.val().position;
      var department = value.val().department;
      m.row.add([
        id,
        name,
        position,
        department
      ]).draw(false);
    });
  });

  
