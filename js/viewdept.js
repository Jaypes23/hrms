 var DeptRef = firebase.database().ref().child("Department");
  DeptRef.orderByChild("deptname").once("value", function(snapshot){
    $('#tb').DataTable({
      columns: [
        {title: "ID"},
        {title: "Name"},
        {title: "Supervisor"},
        {title: "Location"},
      ]
    });
    var m = $('#tb').DataTable();
    snapshot.forEach(function(value){
      var id = value.val().deptid;
      var name = value.val().deptname;
      var supervisor = value.val().deptsupervisor;
      var location = value.val().deptlocation;
      m.row.add([
        id,
        name,
        supervisor,
        location
      ]).draw(false);
    });
  });
