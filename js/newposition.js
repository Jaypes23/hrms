var depthldr = document.getElementById("depthldr");
var optionVal = new Array();

var myVar = localStorage['myKey'] || 'defaultValue';
console.log(myVar);
var admincomp = "";

var CheckRef = firebase.database().ref().child("Users").child(myVar);
CheckRef.once("value", function(snapshot){
      admincomp = snapshot.val().admincomp;
      console.log(admincomp);
   });

setTimeout(function(){
var DeptRef = firebase.database().ref().child("Company").child(admincomp).child("Department");
DeptRef.once("value", function(snapshot){
   snapshot.forEach(function(value){
      var deptname = value.val().deptname;
      optionVal.push(String(deptname));
      console.log(deptname);
   });
 });
}, 5000);

 setTimeout(function(){
   for(var i = 0; i < optionVal.length; i++){
       depthldr.options.add(new Option(optionVal[i], optionVal[i]));
   }
}, 10000);

function AddPosition(){
  var firebaseRef = firebase.database().ref();
  window.alert("Position Added!");

  var randomNum = Math.floor(Math.random() * 1000) + 0;
  
  var PosRoot = firebaseRef.child("Company").child(admincomp).child("Position").child(String(randomNum));

  PosRoot.set({
  posid: String(randomNum),
  posname: document.getElementById("posname").value,
  posdesc: document.getElementById("posdesc").value,
  posdept:  document.getElementById("depthldr").value,
  poswork: document.getElementById("poswork").value,
  posmethod: document.getElementById("posmethod").value,
  poswage: document.getElementById("poswage").value
  })
  .catch(function (error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      console.log("error code: " + errorCode);
      console.log("error message: " + errorMessage);
      window.alert(errorMessage);
  });
}
